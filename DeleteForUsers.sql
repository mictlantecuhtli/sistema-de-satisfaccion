/*
*Coolaborators: Fatima_Azucena_Martinez_Cadena and Raul_Hernandez_Lopez_@Neo
*Date: 30_12_2021
*Description: stored procedure for inserting data to the users table
*/

DELIMITER //
CREATE OR REPLACE DeleteForUsers(
	in IDUSER VARCHAR(30)
)
BEGIN
DELETE FROM users WHERE user_id = IDUSER;
END
DELIMITER;
