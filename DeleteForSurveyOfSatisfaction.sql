/*
*Coolaborators: Fatima_Azucena_Martinez_Cadena and Raul_Hernandez_Lopez_@Neo
*Date: 03_01_2022
*Description: stored procedure for inserting data to the users table
*/

DELIMITER //
CREATE OR REPLACE DeleteForSurveySatisfaction (
        in IDSATISFACTIONSURVEY VARCHAR(30)
)
BEGIN
DELETE FROM satisfaction_survey WHERE id_satisfaction_survey = IDSATISFACTIONSURVEY;
END
DELIMITER;
~           
