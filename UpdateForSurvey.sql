/*
*Coolaborators: Fatima_Azucena_Martinez_Cadena and Raul_Hernandez_Lopez_@Neo
*Date: 30_12_2021
*Description: stored procedure for inserting data to the users table
*/

DELIMITER //
CREATE OR REPLACE PROCEDURE UpdateForSurvey (
	in SURVEYID VARCHAR(30), in NAMESURVEY INT(1)
)
BEGIN
        UPDATE survey 
	SET name_survey = NAMESURVEY
	WHERE id_survey = SURVEID;
END //
DELIMITER ;

