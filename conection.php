<!--Fátima Azucena MC and Raúl Hérnandez Lopez-->
<!--Email: fatimaazucenamartinez274@gmail.com-->
<!--13_01_2022-->

<!--Realizar un web service en PHP, haciendo uso de Apache, el cual nos
retorne un mensaje, en este caso, Hola mundo-->

<?php
	if($_SERVER["REQUEST_METHOD"] == "GET" ){
		
		//Esta variable se obtendra del formulario de otro archivo,
		//se almacenara aqui
		$valorImp= $_GET["variable"];
		
		//Se verifica que la variable este vacio, si es el caso
		//imprime esa variable, posteriormente header imprime un 
		//mensaje de que la operacion fue un exito
		if(!empty($valorImp)){
			echo $valorImp;
			header("HTTP/1.1 200 OK");
			echo json_encode($valorImp);
		}
		else {
			header("HTTP/1.1 204 Sin contenido");
		}
	}
?>
