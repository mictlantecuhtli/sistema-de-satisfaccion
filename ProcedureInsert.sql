/*###################################################
#Name: Fatima Azucena MC                            #
#Date: 27_12_2021                                   #
#email: fatimaazucenamartinez274@gmail.com          #
###################################################*/

DELIMITTER $$

/**/
CREATE PROCEDURE insert_procedure_user (
	in Id_User VARCHAR(30),
	in Type_Of_User VARCHAR(30),
	in Name_Of_User VARCHAR(100),
        in Password VARCHAR(30),
        in Status VARCHAR(10)
)
BEGIN 
INSERT TO users(id_user, type_of_user, name_of_user, password, status) values (Id_User, Type_Of_User, Name_Of_User, Password, Status);
END

CREATE PROCEDURE insert_procedure_user (
	in Id_Poll VARCHAR(30),
        in Dt DATETIME,
        in Nevel_Satisfaction SMALLINT,
        in Coment VARCHAR(120),
        in Folio_Ticket VARCHAR(30);

)
BEGIN
INSERT TO poll(id_poll, dt, nevel_satisfaction, coment, folio_ticket) values (Id_Poll, Dt, Nevel_Satisfaction, Coment, Folio_Ticket);
END

